from __future__ import print_function
import pandas as pd
from df2gspread import gspread2df as g2d
from df2gspread import df2gspread as d2g
import re
from datetime import datetime, timedelta
    
def get_credentials(credentials_path):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    # If modifying these scopes, delete the file token.json.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                credentials_path, SCOPES)
            
            creds = flow.run_console()
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    return creds

if __name__ == '__main__':
    
    current_date = datetime.today()- timedelta(days=3)
    current_date = current_date.strftime('%Y_%m_%d')
    print('Step 1. read offer pool from marketing Gsheets')
    gsheet_id_marketing = '1QFIizInp6ARVPDtMrXRrVqL1jpI4g0xxC4xNRDj7NBQ'
    gsheet_name_marketing = 'Offer pool'
    df_marketing = g2d.download(gfile=gsheet_id_marketing, wks_name=gsheet_name_marketing, col_names=True, row_names=False)
    df_marketing.to_csv(f'df_marketing_{current_date}.csv', index=False)
    
    print('Step 2. read offer pool from supermarket Gsheets')
    gsheet_id_super = '16iKuHSDHGGIbZB5YMhvs0A0eNV7F9F4HILy2F6vIcYA'
    gsheet_name_super = 'offer_pool_backup'
    df_super = g2d.download(gfile=gsheet_id_super, wks_name=gsheet_name_super, col_names=True, row_names=False)
    df_super.to_csv(f'df_super_{current_date}.csv', index=False)
    
    
    print('Step 3. write offer pool to Joe backup Gsheets')
    gsheet_id_backup = '1h_EqljLzzFotsBy6WdMozgm9DZBq0WikQ6jN0th5fYY'
    gsheet_name_backup = current_date
    d2g.upload(df_super, gfile=gsheet_id_backup, wks_name=gsheet_name_backup, col_names=True, row_names=False, clean=True)
    print('Step 3. backup to Joe Gsheets done')
    
    print('Step 4. transfering marketing offer pool to supermarket offer pool')
    # select latest campaing_start_date
    df_marketing.campaign_start_date = pd.to_datetime(df_marketing.campaign_start_date, format='%d-%m-%Y')
    target_date = df_marketing.campaign_start_date.max()
    df_marketing = df_marketing[df_marketing['campaign_start_date']==target_date]
    # df_marketing['Query'] = df_marketing.CampaignProfile
    # clear some columns
    cols_need_to_clear = ['campaign_start_date',
                            'campaign_end_date',
                            'Activation_Start_Date\n',
                            'Activation_End_Date\n',
                            'Offer_id_1_start_date',
                            'Offer_id_1_end_date',
                            'Offer_id_2_start_date',
                            'Offer_id_2_end_date',
                            'Offer_id_3_start_date',
                            'Offer_id_3_end_date',
                            'Offer_id_4_start_date',
                            'Offer_id_4_end_date', 
                            'email_send_date']
    for col in cols_need_to_clear:
        df_marketing[col] = ''
    df_marketing.loc[:, 'total_wow_rewards_Offer_id_4'] = df_marketing.loc[:, 'total_wow_rewards_Offer_id_4'].str.replace(',', '')
    df_marketing.loc[df_marketing.campaign_type=='MP', 'spend_hurdle']= 0.98
    
    
    df_marketing['factorize_filter'] = ''
    cond_idx = (df_marketing.AVG_SPEND_BAND_L == '')
    df_marketing.loc[cond_idx, 'factorize_filter'] = df_marketing.campaign_code + df_marketing.sales_channel_type_desc
    df_marketing.loc[~cond_idx, 'factorize_filter'] = df_marketing.campaign_code + df_marketing.sales_channel_type_desc + df_marketing.AVG_SPEND_BAND_L + df_marketing.AVG_SPEND_BAND_H
    fact_list = df_marketing.factorize_filter.unique()
    for fact in fact_list:
        cond_idx = (df_marketing.factorize_filter == fact)
        df_marketing.loc[cond_idx, 'Duplicate_Occurence'] = pd.factorize(df_marketing.loc[cond_idx].index)[0] + 1
        
    for row_idx in range(df_marketing.shape[0]):
        dup = df_marketing.iloc[row_idx, :].Duplicate_Occurence
        df_marketing.iloc[row_idx, :].Query = re.sub(
            "'\d' as Duplicate_Occurence UNION ALL", f"'{dup}' as Duplicate_Occurence UNION ALL", df_marketing.iloc[row_idx, :].Query)
    df_marketing.drop('factorize_filter', axis=1, inplace=True)
    
    query_cols_list = ['campaign_code',
                       'sales_channel_type_desc',
                       'segment_code',
                       'Template_id',
                       'campaign_type',
                       'campaign_length',
                       'AVG_SPEND_BAND_L',
                       'AVG_SPEND_BAND_H',
                       'spend_hurdle',
                       'total_wow_rewards',
                       'spend_hurdle_Offer_id_2',
                       'total_wow_rewards_Offer_id_2',
                       'spend_hurdle_Offer_id_3',
                       'total_wow_rewards_Offer_id_3',
                       'spend_hurdle_Offer_id_4',
                       'total_wow_rewards_Offer_id_4',
                       'campaign_start_date',
                       'campaign_end_date',
                       'Activation_Start_Date\n',
                       'Activation_End_Date\n',
                       'Single_Multi',
                       'wow_offer_id_w1',
                       'Offer_id_1_start_date',
                       'Offer_id_1_end_date',
                       'Offer_id_1_duration',
                       'DOC_offer_id_W1',
                       'wow_offer_id_w2',
                       'Offer_id_2_start_date',
                       'Offer_id_2_end_date',
                       'Offer_id_2_duration',
                       'DOC_offer_id_W2',
                       'wow_offer_id_w3',
                       'Offer_id_3_start_date',
                       'Offer_id_3_end_date',
                       'Offer_id_3_duration',
                       'DOC_offer_id_W3',
                       'wow_offer_id_w4',
                       'Offer_id_4_start_date',
                       'Offer_id_4_end_date',
                       'Offer_id_4_duration',
                       'DOC_offer_id_W4',
                       'User_Activation_type ',
                       'Fake_activation_ind',
                       'Redemption_limit',
                       'sales_channel_type',
                       'Offer_Type_CD',
                       'email_send_date',
                       'Match_key',
                       'Duplicate_Occurence']
    
    df_marketing.Query = 'SELECT '
    for col in query_cols_list:
        df_marketing.Query += "'" + df_marketing[col].astype(str) + f"' as {col}, "
        
    df_marketing.Query += 'UNION ALL'
    df_marketing.Query = df_marketing.Query.str.replace('\n', '')
    df_marketing.Query = df_marketing.Query.str.replace('Duplicate_Occurence,', 'Duplicate_Occurence')
    df_marketing.iloc[-1].Query = df_marketing.iloc[-1].Query.replace('UNION ALL', '')
    
    print('Step 5. write offer pool to supermarket Gsheets #TODO')
    gsheet_id_backup = '1h_EqljLzzFotsBy6WdMozgm9DZBq0WikQ6jN0th5fYY'
    gsheet_name_backup = current_date + '_new'
    d2g.upload(df_marketing, gfile=gsheet_id_backup, wks_name=gsheet_name_backup, col_names=True, row_names=False, clean=True)
    df_marketing.to_csv('test.csv', index=False)
    
    print('Step 5. write offer pool to supermarket Gsheets #TODO')
    gsheet_id_super = '16iKuHSDHGGIbZB5YMhvs0A0eNV7F9F4HILy2F6vIcYA'
    gsheet_name_super = 'offer_pool'
    
    
    cols_ = df_marketing.columns
    cols_ = cols_.insert(0, 'validation_end_date')
    cols_ = cols_.insert(0, 'validation_start_date')
    print(cols_)
    df_marketing['validation_start_date'] = '2020-08-30 00:00:00'
    df_marketing['validation_end_date'] = '2099-12-31 23:59:59'
    df_marketing = df_marketing[cols_]
    d2g.upload(df_marketing, gfile=gsheet_id_super, wks_name=gsheet_name_super, col_names=True, row_names=False, clean=True)
    df_marketing.to_csv('test.csv', index=False)
    
    
    
    
    
    
    
    
    
    
    