# Google Sheets Synchronization Tool #
### Version: 0.8

### Description:
Synchronize offer pool gsheets from 
**"Inclusion Exclusion for Melon"** to 
**"Super - decision pool - PROD"** 
### Requirements:
#### python packages
pip install -r requirements.txt
#### authentications
Gsheet API key: (ask squad leader for permission, [credentials manage dashboard](https://console.cloud.google.com/apis/api/sheets.googleapis.com/credentials?project=wx-bq-poc))

Gsheets access: (ask [Joe Nguyen](jnguyen11@woolworths.com.au) for access):

* [Inclusion Exclusion for Melon](https://docs.google.com/spreadsheets/d/1QFIizInp6ARVPDtMrXRrVqL1jpI4g0xxC4xNRDj7NBQ/edit?ts=602cad51#gid=1786638710)
* [Super - decision pool - PROD](https://docs.google.com/spreadsheets/d/16iKuHSDHGGIbZB5YMhvs0A0eNV7F9F4HILy2F6vIcYA/edit?ts=602cad46#gid=1138827126)
* [HIST 2 - Super - decision pool - PROD
](https://docs.google.com/spreadsheets/d/1h_EqljLzzFotsBy6WdMozgm9DZBq0WikQ6jN0th5fYY/edit?ts=602caf8a#gid=1143950196)

### How to run:

For local machine, simply run the code. Then follow the instruction to authorize the API to access your google sheets
```
python decision_pool_fortnightly_update.py
```

For remote server, insert following lines in the site-packages
```
flags.noauth_local_webserver = True
```

For example, in my working environment
```
python -m site
cd /home/jovyan/my-conda-envs/venv_aliu2_dev/lib/python3.9/site-packages/df2gspread
nano utils.py
```
go to line 74
insert **flags.noauth_local_webserver = True**
```
    try:
        import argparse
        flags = argparse.ArgumentParser(
            parents=[tools.argparser]).parse_known_args()[0]
    except ImportError:
        flags = None
        logr.error(
            'Unable to parse oauth2client args; `pip install argparse`')
```
so we have
```
    try:
        import argparse
        flags = argparse.ArgumentParser(
            parents=[tools.argparser]).parse_known_args()[0]
        flags.noauth_local_webserver = True
    except ImportError:
        flags = None
        logr.error(
            'Unable to parse oauth2client args; `pip install argparse`')
```
then ctl+x, y, enter
restart kernel and import df2gspread should work.
### Who do I talk to? ###

* Anjin Liu
* email: aliu2@woolworths.com.au